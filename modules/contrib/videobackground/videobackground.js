/**
 * @file
 * Provides a video display inculded css selectors.
 */

(function ($) {
    'use strict';

    Drupal.behaviors.videobackground = {
        attach: function (context, settings) {
          var include = settings.video_background.include;
          $(include, context).each(function (index, value) {
          // Get video path.
          var videPath = settings.video_background.path;
          // Set options.
          var options = settings.video_background.options;
          Drupal.theme('videoBackgroundTrigger', include, videPath, options);
          });
        }
    };

    Drupal.theme.videoBackgroundTrigger = function (targetSelectors, videPath, object) {
      var options = new Array();
      var bg = new Array();
      var arr = new Array();
      $.each(object, function (index, value) {
        options.push(index + ':' + value)
      });
      $.each(videPath, function (index, value) {
        if (typeof (value) !== "undefined" && value) {
        arr = value.split('-');
        bg.push(index + ':' + arr[1]);
        }
      });
      return $(targetSelectors).attr({
        "data-vide-bg": bg,
        "data-vide-options": options,
      });
    };

})(jQuery, Drupal);
