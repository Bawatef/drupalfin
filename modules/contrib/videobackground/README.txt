VideoBackground:

Easy as hell module for play video background.

This module relies on the jQuery vide plugin.

## Notes

* All modern desktop browsers are supported.
* IE9+
* iOS plays video from a browser only in the native player.
So video for iOS is disabled, only fullscreen poster will be used
* Some android devices play video, some not — go figure.
So video for android is disabled, only fullscreen poster will be used.

Installation

Place the videobackground module into modules directory
Enable this module by navigating to: Administration > Extend
Go to module configuration page under the visibility settings add CSS selectors
for allowing video to play in background.
Prepare your video in several formats like '.webm', '.mp4' for cross browser
compatibility, also add a poster with .jpg, .png or .gif extension:

    path/
    ├── to/
    │ ├── video.mp4
    │ ├── video.ogv
    │ ├── video.webm
    │ └── video.jpg

    Video and poster must have the same name.
    Setup video background options, if you need it. By default video is muted,
    looped and starts automatically.
